/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnendl.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/30 14:51:50 by mmeisson          #+#    #+#             */
/*   Updated: 2016/04/30 15:07:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

void		ft_putnendl(const char *str, size_t n)
{
	write(1, str, ft_strnlen(str, n));
	write(1, "\n", n);
}
