/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabappend.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/31 11:17:41 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 18:22:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**ft_tabnappend(char ***tab, const char *str, size_t len)
{
	size_t		i;
	char		**array;

	array = *tab;
	if (array == NULL)
		i = 0;
	else
		i = ft_tablen((const char **)array);
	if (!(*tab = malloc(sizeof(*tab) * (i + 2))))
		return (NULL);
	i = 0;
	while (array[i] != NULL)
	{
		(*tab)[i] = array[i];
		i++;
	}
	(*tab)[i++] = ft_strndup(str, len);
	(*tab)[i] = NULL;
	free(array);
	return (*tab);
}
