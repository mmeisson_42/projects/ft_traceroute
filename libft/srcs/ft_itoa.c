/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:58:30 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:04:22 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static int	get_size(int n)
{
	unsigned int	o;
	int				i;

	o = (n >= 0) ? n : -n;
	i = (n >= 0) ? 1 : 2;
	while (o > 9)
	{
		o /= 10;
		i++;
	}
	return (i);
}

char		*ft_itoa(int n)
{
	int				i;
	char			*tab;
	unsigned int	o;

	i = get_size(n);
	if (!(tab = malloc(sizeof(char) * (i + 1))))
		return (NULL);
	o = (n > 0) ? n : -n;
	tab[i--] = 0;
	if (n < 0)
		tab[0] = '-';
	while (o > 9)
	{
		tab[i--] = o % 10 + 48;
		o /= 10;
	}
	tab[i--] = o + 48;
	return (tab);
}
