#include <stdio.h>
#include <stdlib.h>

void	helper(const char *command, int exit_status)
{
	printf("Usage: %s [-h] [ -t first_ttl ] [ -m first_ttl ] [ -M last_ttl ] [ -W timeout ] host\n", command);
	exit(exit_status);
}
