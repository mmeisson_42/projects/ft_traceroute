#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include "libft.h"
#include "checksum.h"
#include "ping.h"

int	ping(
	s_context *ctx,
	struct sockaddr_in *dest,
	s_ping packet,
	u_short sequence,
	struct timeval *tv
)
{
	packet.icmp.icmp_seq = htons(sequence);
	packet.icmp.icmp_cksum = 0;
	packet.icmp.icmp_cksum = checksum(&packet.icmp, sizeof packet.icmp + sizeof packet.data);
	gettimeofday(tv, NULL);
	if (sendto(
		ctx->socket_fd,
		&packet,
		sizeof(packet),
		0,
		(struct sockaddr *)dest,
		sizeof *dest) == -1
	)
	{
		dprintf(2, "Sendto failed\n");
		return (-1);
	}
	return (0);
}
