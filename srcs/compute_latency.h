#ifndef COMPUTE_LATENCY_H
# define COMPUTE_LATENCY_H

double		compute_latency(struct timeval *origin, struct timeval *last);

#endif
