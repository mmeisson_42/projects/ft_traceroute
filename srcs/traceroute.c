#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "libft.h"
#include "ft_traceroute.h"
#include "compute_latency.h"
#include "ping.h"
#include "send_ping.h"
#include "get_host_by_addr.h"

static void		print_route(int ttl, const char *remote_addr, double timeval[3])
{
	char		*resolved = get_host_by_addr(remote_addr);

	printf(
		"%-4d%s (%s) ",
		ttl,
		resolved != NULL ? resolved : remote_addr,
		remote_addr
	);
	for (int j = 0; j < 3; j++)
	{
		if (timeval[j] >= 0.0)
		{
			printf(" %.3f ms ", timeval[j]);
		}
		else
		{
			printf(" * ");
		}
	}
	printf("\n");
}

void		traceroute(s_context *ctx)
{
	if (ctx->first_ttl < 1 || ctx->last_ttl > 255 || ctx->first_ttl > ctx->last_ttl)
	{
		dprintf(2, "min or max ttl has not good values\n");
		exit(1);
	}
	printf(
			"traceroute to %s (%s), %d hops max, %lu bytes packets\n",
			ctx->host_name,
			ctx->addr,
			ctx->last_ttl,
			DATA_LEN
		  );

	int	host_reached = 0;

	for (int ttl = ctx->first_ttl; ttl <= ctx->last_ttl && host_reached == 0; ttl++)
	{
		char		*remote_addr = NULL;
		double		timeval[3];
		char		buffer[INET_ADDRSTRLEN];

		int r = setsockopt(ctx->socket_fd, IPPROTO_IP, IP_TTL, &ttl, sizeof ttl);
		if (r == -1)
		{
			dprintf(2, "Error while setting ttl\n");
			continue ;
		}
		for (int j = 0; j < 3; j++)
		{
			s_meta_ping *meta = send_ping(ctx, j);

			if (meta != NULL)
			{
				if (meta->pong.icmph.type == ICMP_ECHOREPLY)
				{
					host_reached = 1;
				}
				if (remote_addr == NULL)
				{
					if (inet_ntop(AF_INET, &meta->pong.ip.ip_src, buffer, sizeof buffer) != NULL)
					{
						remote_addr = ft_strdup(buffer);
					}
				}
				timeval[j] = compute_latency(&meta->from, &meta->to);
				free(meta);
			}
			else
			{
				timeval[j] = -1.0;
			}
		}
		if (remote_addr == NULL)
		{
			puts("* * *");
		}
		else
		{
			print_route(ttl, remote_addr, timeval);
			free(remote_addr);
		}
	}
}
