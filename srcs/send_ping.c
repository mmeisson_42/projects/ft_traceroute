#include <stdio.h>
#include <arpa/inet.h>
#include "ping.h"
#include "pong.h"
#include "libft.h"
#include "send_ping.h"

s_meta_ping		*send_ping(struct s_context *ctx, int sequence)
{
	struct s_ping packet = {
		.icmp = {
			.icmp_type = ICMP_ECHO,
			.icmp_code = 0,
			.icmp_id = (u_int16_t)getpid(),
		},
	};
	struct sockaddr_in dest = { .sin_family = AF_INET };
	struct s_meta_ping		*meta = ft_memalloc(sizeof *meta);

	if (meta == NULL)
	{
		return NULL;
	}

	ft_memset(&packet.icmp.icmp_data, 0, DATA_LEN);
	inet_pton(AF_INET, ctx->addr, &dest.sin_addr);

	if (ping(ctx, &dest, packet, sequence, &meta->from) != -1)
	{
		struct s_pong	*ponged = pong(ctx, &meta->to);

		if (ponged != NULL)
		{
			ft_memcpy(&meta->pong, ponged, sizeof *ponged);
			free(ponged);
			return meta;
		}
	}
	else
	{
		printf("failed to ping\n");
	}
	free(meta);
	return NULL;
}
