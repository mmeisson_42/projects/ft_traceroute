#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include "ft_traceroute.h"
#include "traceroute.h"
#include "resolve_host.h"
#include "ping.h"
#include "libft.h"
#include "helper.h"

static int	get_value_option(int index, char **argv)
{
	int		value = -1;
	const char *str_option = argv[index + 1];

	if (str_option != NULL)
	{
		for (int i = 0; str_option[i]; i++) { if (!ft_isdigit(str_option[i])) { return -1; } }
		value = ft_atoi(str_option);
	}
	return value;
}

static char	*parse_options(int argc, char **argv, s_context *ctx)
{
	char		*command = argv[0];
	int			i;

	for (i = 1; i < argc; i++)
	{
		if (argv[i][0] != '-')
		{
			break ;
		}
		if (ft_strcmp(argv[i], "--") == 0)
		{
			i++;
			break ;
		}
		int to_break = 0;
		for (int j = 1; argv[i][j] && to_break == 0; j++)
		{
			char		opt = argv[i][j];

			switch (opt)
			{
				case 'h':
					helper(command, 0);
					break ;
				case 'f':
					ctx->first_ttl = get_value_option(i, argv);
					if (ctx->first_ttl == -1 || argv[i++][j + 1] != 0)
						helper(command, 1);
					to_break = 1;
					break;
				case 'm':
					ctx->last_ttl = get_value_option(i, argv);
					if (ctx->last_ttl == -1 || argv[i++][j + 1] != 0)
						helper(command, 1);
					to_break = 1;
					break;
				case 'w':
					ctx->timeout = get_value_option(i, argv);
					if (ctx->timeout == -1 || argv[i++][j + 1] != 0)
						helper(command, 1);
					to_break = 1;
					break;
				default:
					helper(command, 1);
			}
		}
	}
	if (argv[i] == NULL || argv[i + 1] != NULL)
	{
		helper(command, 1);
	}
	return (argv[i]);
}

int		main(int argc, char **argv)
{
	s_context	ctx = {
		.first_ttl = FIRST_DEFAULT_TTL,
		.last_ttl = LAST_DEFAULT_TTL,
		.timeout = DEFAULT_TIMEOUT
	};

	/* Data initialisation block
	*/
	ctx.host_name = parse_options(argc, argv, &ctx);
	ctx.addr = resolve_host(ctx.host_name);
	if (ctx.addr == NULL)
	{
		dprintf(
			2,
			"ping: canot resolve %s: Unknown host\n",
			ctx.host_name
		);
		return (EXIT_FAILURE);
	}

	/* Socket initialisation block
	*/
	ctx.socket_fd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (ctx.socket_fd == -1)
	{
		dprintf(2, "socket error\n");
		exit(EXIT_FAILURE);
	}
	struct timeval timeout = { .tv_sec = ctx.timeout };
	int r = setsockopt(ctx.socket_fd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof timeout);
	if (r == -1)
	{
		dprintf(2, "Error while setting timeout\n");
	}

	/* Running loop
	*/
	traceroute(&ctx);
	return (EXIT_SUCCESS);
}
