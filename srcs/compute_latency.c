#include <sys/time.h>
#include <stdlib.h>
#include "ft_traceroute.h"

double	compute_latency(struct timeval *origin, struct timeval *last)
{
	long			usec_latency;

	usec_latency = (last->tv_sec * USEC_PER_SEC + last->tv_usec) -
		(origin->tv_sec * USEC_PER_SEC + origin->tv_usec);
	return usec_latency / (double)USEC_PER_MSEC;
}

