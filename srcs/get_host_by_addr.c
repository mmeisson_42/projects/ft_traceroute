#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include "libft.h"
#include "get_host_by_addr.h"

char	*get_host_by_addr(const char *addr)
{
	struct in_addr		addr4;
	char				*host_name = NULL;

	if (inet_pton(AF_INET, addr, &addr4) >= 0)
	{
		struct hostent	* h = gethostbyaddr(&addr4, sizeof addr4, AF_INET);
		if (h != NULL)
		{
			host_name = ft_strdup(h->h_name);
		}
	}
	return host_name;
}
