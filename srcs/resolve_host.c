#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "libft.h"
#include "ft_traceroute.h"

char		*resolve_host(const char *addr)
{
	char						resolved[INET_ADDRSTRLEN] = {0};
	static struct addrinfo		*addrinfo = NULL;

	if (addrinfo == NULL)
	{
		int got_addr = getaddrinfo(addr, NULL, NULL, &addrinfo);
		if (got_addr != 0 || addrinfo == NULL)
		{
			return NULL;
		}
	}
	inet_ntop(
		AF_INET,
		&((struct sockaddr_in *)addrinfo->ai_addr)->sin_addr,
		resolved,
		sizeof resolved
	);
	return ft_memdup(resolved, sizeof resolved);
}
