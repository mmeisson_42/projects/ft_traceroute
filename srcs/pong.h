#ifndef PONG_H
# define PONG_H

# include <netinet/ip.h>
# include <netinet/ip_icmp.h>
# include <sys/time.h>
# include "ft_traceroute.h"

# define HAS_KERNEL_BUG

typedef struct	s_pong
{
	struct ip		ip;
	struct icmphdr	icmph;
	char		data[DATA_LEN];
}		s_pong;

s_pong		*pong(s_context *ctx, struct timeval *tv);

#endif
