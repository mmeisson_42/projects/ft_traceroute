#ifndef SEND_PING_H
# define SEND_PING_H

# include <sys/time.h>
# include "pong.h"

typedef struct	s_meta_ping
{
	s_pong			pong;
	struct timeval	from;
	struct timeval	to;
}				s_meta_ping;

s_meta_ping		*send_ping(s_context *ctx, int sequence);

#endif
