#ifndef PING_H
# define PING_H

# include <sys/types.h>
# include <sys/socket.h>
# include <sys/time.h>
# include <netinet/ip_icmp.h>
# include "ft_traceroute.h"

typedef struct	s_ping
{
	struct icmp		icmp;
	char			data[ADDITIONAL_ICMP];
}		s_ping;

int	ping(
	s_context *ctx,
	struct sockaddr_in *dest,
	s_ping packet,
	u_short sequence,
	struct timeval *tv
);

#endif
