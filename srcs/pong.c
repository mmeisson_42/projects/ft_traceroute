#include <netinet/ip_icmp.h>
#include <sys/time.h>
#include "pong.h"
#include "libft.h"

s_pong		*pong(s_context *ctx, struct timeval *tv)
{
	s_pong		pong = {0};
	struct sockaddr_in	sin;
	struct iovec		iov = {
		.iov_base = &pong,
		.iov_len = sizeof pong,
	};
	struct msghdr		msg = {
		.msg_iov = &iov,
		.msg_iovlen = 1,
		.msg_name = &sin,
		.msg_namelen = sizeof sin,
	};
	s_pong		*res = NULL;
	int			res_recv;

	do
	{
		res_recv = recvmsg(ctx->socket_fd, &msg, 0);
		if (res_recv != -1)
		{
			if (pong.icmph.type != ICMP_ECHO)
			{
				gettimeofday(tv, NULL);
				res = ft_memdup(&pong, sizeof pong);
			}
		}
	}
	while (res == NULL && res_recv >= 0);

	return res;
}
