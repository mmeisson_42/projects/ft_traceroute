#ifndef FT_TRACEROUTE_H
# define FT_TRACEROUTE_H

# include <netinet/ip_icmp.h>

# define TYPE_ECHO			8
# define DEFAULT_TIMEOUT	1
# define FIRST_DEFAULT_TTL	1
# define LAST_DEFAULT_TTL	30

# define AI_ADDRLEN			32

# define ADDITIONAL_ICMP	40

# define DATA_LEN (ADDITIONAL_ICMP + sizeof ((struct icmp *)0)->icmp_dun)

# define USEC_PER_SEC		1000000
# define USEC_PER_MSEC		1000

# define BYTE_TO_WORD(len) ((len) >> 2)

typedef struct	s_context
{
	char			*host_name;
	char			*addr;
	int				socket_fd;

	int				options;
	int				timeout;
	int				first_ttl;
	int				last_ttl;
}		s_context;

#endif
